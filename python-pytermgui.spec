%global srcname pytermgui
%global sum A Python TUI framework 

Name:           python-%{srcname}
Version:        7.3.0
Release:        2%{?dist}
Summary:        %{sum}

License:        MIT
URL:            https://github.com/bczsalba/pytermgui
Source0:        %{url}/archive/v%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  pyproject-rpm-macros
BuildRequires:  python%{python3_pkgversion}-hatch-fancy-pypi-readme
BuildRequires:  python%{python3_pkgversion}-typing-extensions


%description
Python TUI framework with mouse support, modular widget system, customizable and
rapid terminal markup language and more!


%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{sum}

%description -n python%{python3_pkgversion}-%{srcname}
Python TUI framework with mouse support, modular widget system, customizable and
rapid terminal markup language and more!


%prep
%autosetup -p1 -n %{srcname}-%{version}


%generate_buildrequires
%pyproject_buildrequires


%build
%pyproject_wheel
 
%install
%pyproject_install


%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE
# % doc CREDITS HISTORY.rst README.rst
%{_bindir}/ptg
%{python3_sitelib}/%{srcname}/
%{python3_sitelib}/%{srcname}-%{version}.dist-info/
%exclude %{python3_sitelib}/tests


%changelog
* Wed Feb 22 2023 Freeman Zhang <zhanggyb@163.com> - 7.3.0-2
- Use correct URL of source package

* Wed Feb 22 2023 Freeman Zhang <zhanggyb@163.com> - 7.3.0-1
- Initial RPM build
