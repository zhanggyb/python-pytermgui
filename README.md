# pytermgui

Python TUI framework with mouse support, modular widget system, customizable and
rapid terminal markup language and more!

<p style="text-align:center">
<img src="https://raw.githubusercontent.com/bczsalba/pytermgui/master/assets/readme/screenshot.png" alt="screenshot">
</p>
